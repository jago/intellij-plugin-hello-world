import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.editor.Caret;
import com.intellij.openapi.editor.CaretModel;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class NaturalLanguageToCode extends AnAction {

    @Override
    public void actionPerformed(AnActionEvent e) {
        // We can get the language of the current file.
        // Language lang = e.getData(CommonDataKeys.PSI_FILE).getLanguage();

        // This is how we access text in the editor.
        // Docs: https://www.jetbrains.org/intellij/sdk/docs/tutorials/editor_basics/working_with_text.html
        final Editor editor = e.getRequiredData(CommonDataKeys.EDITOR);
        final Project project = e.getRequiredData(CommonDataKeys.PROJECT);
        final Document document = editor.getDocument();

        CaretModel caretModel = editor.getCaretModel();
        Caret currentCaret = caretModel.getCurrentCaret();

        // Getting the selected text that we can translate to code.
        String selectedText = currentCaret.getSelectedText();
        String code = "";
//        code = englishToCode(selectedText);

        // Getting text from a remote resource
        try {
            code = englishToCodeFromRemoteResource(selectedText);
        } catch (Exception err) {
            err.printStackTrace();
        }

        // But for the actual replacement, we need the start and end positions.
        int start = currentCaret.getSelectionStart();
        int end = currentCaret.getSelectionEnd();
        String finalCode = code;
        WriteCommandAction.runWriteCommandAction(project, () -> document.replaceString(start, end, finalCode));

        currentCaret.removeSelection();
    }

    @Override
    public void update(AnActionEvent e) {
        Editor editor = e.getRequiredData(CommonDataKeys.EDITOR);
        CaretModel caretModel = editor.getCaretModel();
        e.getPresentation().setEnabledAndVisible(caretModel.getCurrentCaret().hasSelection());
    }

    // Mocking the translation from English to code.
    private String englishToCode(String text) {
        return "for (int i = 0; i < 10; i++) {";
    }

    private String englishToCodeFromRemoteResource(String text) throws Exception {

        String url = "https://jsonplaceholder.typicode.com/todos/1";

        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(url);
        HttpResponse response = client.execute(request);

        System.out.println("\nSending GET request to URL : " + url);
        System.out.println("Response code : " + response.getStatusLine().getStatusCode());

        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        JsonObject jsonObject = (new JsonParser()).parse(result.toString()).getAsJsonObject();
        return jsonObject.get("title").getAsString();
    }
}
